package com.pamaspay.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Locale;

/**
 * Handles money transactions
 * http://stackoverflow.com/a/2449313/1402526
 */

public class Money {

    private static final Currency USD = Currency.getInstance("USD");
    private static final Currency MYR = Currency.getInstance("MYR");
    private static final RoundingMode DEFAULT_ROUNDING = RoundingMode.HALF_EVEN;

    private BigDecimal amount;
    private Currency currency;

    public static Money dollars(BigDecimal amount) {
        return new Money(amount, USD);
    }

    public static Money ringgit(BigDecimal amount) {
        return new Money(amount, MYR);
    }

    private Money() {}

    private Money(BigDecimal amount, Currency currency) {
        this(amount, currency, DEFAULT_ROUNDING);
    }

    private Money(BigDecimal amount, Currency currency, RoundingMode rounding) {
        this.amount = amount;
        this.currency = currency;

        this.amount = amount.setScale(currency.getDefaultFractionDigits(), rounding);
    }

    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        if (currency.equals(MYR)) {
            if (getAmount().compareTo(BigDecimal.ZERO) < 0) {
                return "(-) RM " + getAmount().abs();
            } else {
                return "RM " + getAmount();
            }
        } else {
            return getCurrency().getSymbol() + " " + getAmount();
        }
    }

    /** Show the money in text format based on currency, e.g. RM4 or $4 **/
    public String toString(Locale locale) {
        if (getAmount().compareTo(BigDecimal.ZERO) < 0) {
            return "(-) " + getCurrency().getSymbol(locale) + " " + getAmount().abs();
        } else {
            return getCurrency().getSymbol(locale) + " " + getAmount();
        }
    }

    /** The addition of one money object with another. Returns the value, but does not add it to
     * the object itself **/
    public Money add(Money m) {
        Money money = new Money();
        if (!currency.equals(m.currency)) {
            throw new UnsupportedOperationException();
        }
        money.currency = m.currency;
        money.amount = m.amount.add(amount);
        return money;
    }
}