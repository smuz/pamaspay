package com.pamaspay.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import com.pamaspay.R
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.math.BigDecimal

class SendMoneyAmountActivity : AppCompatActivity() {

    lateinit var etAmount: EditText
    lateinit var btContinue: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_money)
        etAmount = find(R.id.et_amount)
        btContinue = find(R.id.bt_continue)
        btContinue.setOnClickListener {
            if (!etAmount.text.isNullOrBlank()) {
                val amount: String = etAmount.text.toString()
                if (BigDecimal(amount) <= BigDecimal.ZERO) {
                    toast("Invalid amount")
                } else {
                    startActivity<SendMoneyConfirmActivity>(SendMoneyConfirmActivity.INTENT_AMOUNT to amount)
                }
            } else {
                toast("Invalid amount")
            }
        }
    }
}
