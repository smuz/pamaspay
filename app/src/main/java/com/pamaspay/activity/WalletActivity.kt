package com.pamaspay.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import com.pamaspay.R
import com.pamaspay.utils.Money
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import java.math.BigDecimal

class WalletActivity : AppCompatActivity() {

    lateinit var balance: Money
    lateinit var tvBalance: TextView
    lateinit var btAdd: Button
    lateinit var btSend: Button
    lateinit var btRequest: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)
        supportActionBar?.elevation = 0F
        tvBalance = find(R.id.tv_balance)
        btAdd = find(R.id.bt_add)
        btSend = find(R.id.bt_send)
        btRequest = find(R.id.bt_request)
        balance = Money.ringgit(BigDecimal("100"))
        tvBalance.text = "Balance: " +  balance.toString()
        btAdd.setOnClickListener {  }
        btSend.setOnClickListener {
            startActivity<SendMoneyAmountActivity>()
        }
        btRequest.setOnClickListener {
            startActivity<ReceiveActivity>()
        }
    }
}
