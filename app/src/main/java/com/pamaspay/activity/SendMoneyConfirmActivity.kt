package com.pamaspay.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.pamaspay.R
import com.pamaspay.utils.Money
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import java.math.BigDecimal

class SendMoneyConfirmActivity : AppCompatActivity() {

    lateinit var etAcct: EditText
    lateinit var etNotes: EditText
    lateinit var ivQr: ImageView
    lateinit var btConfirm: Button
    var cashTransfer: Money = Money.ringgit(BigDecimal.ZERO)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_money_confirm)
        etAcct = find(R.id.et_acct)
        etNotes = find(R.id.et_notes)
        ivQr = find(R.id.iv_qr)
        btConfirm = find(R.id.bt_confirm)
        cashTransfer.amount = BigDecimal(intent.extras.getString(INTENT_AMOUNT))
        btConfirm.setOnClickListener {
            var acctNum: String? = null
            var notes: String? = null
            if (!etAcct.text.isNullOrBlank()) {
                acctNum = etAcct.text.toString()
            }
            if (!etNotes.text.isNullOrBlank()) {
                notes = etAcct.text.toString()
            }

            if (!acctNum.isNullOrBlank()) {
                alert("Are you sure you want to transfer $cashTransfer?") {
                    positiveButton("Yes") { transfer(cashTransfer, acctNum!!, notes) }
                    negativeButton("No") { }
                }.show()

            }
        }
    }

    fun transfer(amount: Money, acct: String, notes: String?) {
        toast("Your transfer of ${amount} is successful!")
        finish()
    }

    companion object {
        val INTENT_AMOUNT = "inamount"
    }
}
