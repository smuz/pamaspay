package com.pamaspay;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.parse.Parse;

public class ParseApplication extends Application {

    private Context context = this;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Register your parse models
//        ParseObject.registerSubclass(Address.class);


        Parse.enableLocalDatastore(this);

        // Connect to Parse
        String appId = "vTsvJpHVnV3fPFjwvrxcFadp";
        String server = "http://pamaspay.herokuapp.com/parse";
        Parse.initialize(new Parse.Configuration.Builder(context)
                .applicationId(appId)
                .server(server)
                .build()
        );


        // Connect to OneSignal (push notifications)
//        OneSignal.startInit(this)
//                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
//                .unsubscribeWhenNotificationsAreDisabled(true)
//                .init();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
